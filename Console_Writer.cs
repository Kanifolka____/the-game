using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TXT_Game
{
    public static class GameTextWriter // ����� ��� ������ � �������� 
    {
        private static int speed_dialog = 50; // �������� ������ ������ � ������� 
        private static int speed_mech_message = 70;
        public static int Speed_dialog
        {
            get => speed_dialog;
        }

        public static string SetSpeed(int value) //����� ��� ��������� �������� 
        {
            if (value < 0) // �������� �� ������������� �����( ��������)  
            {
                return "���������� �������� �� ����� ���� �������������.\n" +
                    $"������� ��������: {speed_dialog}.";
            }
            else
            {
                speed_dialog = value;
                return "���������� �������� ��� ������� �������.\n" +
                    $"������� ��������: {speed_dialog}.";
            }
        }

        public static void Dialog_Writer(string str) //����� ��� ������ ������ ������� � �������
        {
            for (int i = 0; i < str.Length; i++)
            {
                Console.Write(str[i]);
                //Console.Beep( 659, 300);
                Thread.Sleep(speed_dialog);
            }
            Console.WriteLine("");
        }
        public static void Mechanical_mes(string[] str) // ����� ��� ������ �������� ���������
        {
            for (int i = 0; i < str.Length; i++)
            {
                Console.WriteLine(str[i]);
                Thread.Sleep(speed_mech_message);
            }
        }
    }

    public static class Mechanical_Message // ����� � ������������ �����������,
    {
        private static string[] first_setting_message = new string[] //���� ��������
        {
            "=========================================|SETTINGS|=========================================",
            "",
            $"������� �������� ��������������� ��������: {TextWriter.Speed_dialog}",
            "",
            "============================================================================================"
        };

        private static string[] second_setting_message = new string[] //������ ��������
        {
            "�� ������ �������� ��������?",
            "������� 'Yes' / 'No' "
        };

        private static string[] game_name = new string[]
        {
            "" //TODO �������� ����, "�������", ��������
        };

        private static string[] message_of_ready = new string[] //������ ������
        {
            "�� ������ ������?",
            "������� ����� 'ready'."
        };
        private static string incorrect_response = "������ ������������ �����"; // ��� ����� ��������� ������ 
        private static string incorrect_answer_ready = "�� ����������� � ����� ����������."; // ��� ����� ��������� ����� ����������

        public static string Incorrect_response
        {
            get => incorrect_response;
        }

        public static string Incorrect_answer_ready
        {
            get => incorrect_answer_ready;
        }
        public static void Give_Setting() //����� ��� ������ ��������
        {
            GameTextWriter.Mechanical_mes(first_setting_message);
        }
        public static void Set_settings() // ����� ��� ������ �������� � �������� �� ����������
        {
            GameTextWriter.Mechanical_mes(first_setting_message);
            GameTextWriter.Mechanical_mes(second_setting_message);
        }

        public static void Start_Game_Message()
        {
            GameTextWriter.Mechanical_mes(message_of_ready);
        }

        public static void Game_init() 
        {
            GameTextWriter.Mechanical_mes(game_name);
            GameTextWriter.Mechanical_mes(message_of_ready);
        }
    }


}
