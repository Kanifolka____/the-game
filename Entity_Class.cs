using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TXT_Game
{
    public abstract class Entity
    {
        private string name;
        private int hp;
        private int money;
        private bool life;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        public int HP
        {
            get { return hp; }
        }

        public int Money
        {
            get { return money; }
            set { money = value; }
        }

        public bool Life
        {
            get { return life; }
        }

        public abstract void Damage(int dmg);
    }


    public class Character : Entity
    {
        public Character(string name, int hp, int money) : base(name, hp, money)
        {
            this.Name = name;
            this.HP = hp;
            this.Money = money;

        }

        public override void Damage(int dmg)
        {
            this.HP -= dmg;
            if (this.HP < 0) 
            {
                this.Life = false;
                this.Money = 0;
                //TODO: �������� � ������
            }
        }
    
        //TODO: �������� ����� ����� 
    
    }


    public class Enemy : Entity
    {
        public Enemy(string name, int hp) : base(name, hp)
        {
            this.Name = name;
            this.HP = hp;

        }

        public override void Damage(int dmg, Character cc)
        {
            this.HP -= dmg;
            if (this.HP < 0)
            {
                //TODO: ���-�� ���������� � ������ (����������� ������)
            }

        }
    }


}
